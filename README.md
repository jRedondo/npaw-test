# Itunes Album Search

## Before starting
- This project is a tool to search albums from Itunes database.
- It searches by the term specified in a search box. To optimize loading times and data usage all responses are downloaded and saved in cache so if the search again with the same term it isn't needed to ask for the data again. After a time X (1 minute) in case of search it will download responses again in case there was any update.

## Table of Contents

- [Folder Structure](#folder-structure)
- [Available Scripts](#available-scripts)
  - [npm start](#npm-start)
  - [npm run build](#npm-run-build)

## Folder Structure

After creation, your project should look like this:

```
my-app/
  README.md
  node_modules/
  package.json
  public/
    index.html
    favicon.ico
  src/
    actions/
      index.js
    components/
		AlbumList.js
		  Album/
			AlbumItem.js
			index.js
			styles.css
    containers/
      AlbumListContainer.js
    reducers/
      album.js
      page.js
      index.js
      term.js
    services/
      transformAlbum.js
    store/
      index.js
    App.css
    App.js
    App.test.js
    index.css
    index.js
```

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](#deployment) for more information.
