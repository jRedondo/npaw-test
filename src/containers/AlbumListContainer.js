import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as actions from './../actions';
import { getAlbumDataFromAlbums , getTerm, getPage } from './../reducers'
import AlbumList from './../components/AlbumList'

class AlbumListContainer extends Component {

    componentDidMount() {
        const { setItunesTerm } = this.props;
        setItunesTerm(this.props.term,1);
    }
    
    handleTermInput = term => {
        const { setItunesTerm } = this.props;
        setItunesTerm(term,1);
    }

    handlePaginator = page => {
        const { setItunesTerm } = this.props;
        setItunesTerm(this.props.term, page);
    } 

    render() {
        const { term, albumData, page, pages } = this.props;
        
        return (
            term &&
            <AlbumList term={term}
                onInputUpdate={this.handleTermInput}
                onPageUpdate={this.handlePaginator}
                albumData={albumData}
                page={page}
                pages={pages}
            />
        );
    }
}

AlbumListContainer.propTypes = {
    onInputUpdate : PropTypes.func,
    onPageUpdate : PropTypes.func,
    albumData : PropTypes.array,   
    page : PropTypes.number.isRequired,
    pages : PropTypes.number.isRequired,
    term: PropTypes.string.isRequired,
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

const mapStateToProps = (state) => ({ 
    term: getTerm(state), 
    albumData: getAlbumDataFromAlbums(state),
    page: getPage(state),
    pages: state.pages,
});

export default connect(mapStateToProps, mapDispatchToProps)(AlbumListContainer);