import { SET_PAGES } from './../actions';

export const pages = (state = {}, action) => {
    switch(action.type){
        case SET_PAGES:
            return action.payload;
        default:
            return state;
    }
}