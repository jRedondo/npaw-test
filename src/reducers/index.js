import { combineReducers } from 'redux';
import { createSelector } from 'reselect';
import { albums, getAlbumDataFromAlbums as _getAlbumDataFromAlbums } from './albums';
import { term } from './term';
import { page } from './page';
import { pages } from './pages';

export default combineReducers({
    albums,
    term, 
    page,
    pages,
});

export const getTerm = createSelector(state => state.term, term => term);

export const getPage = createSelector(state => state.page, page => page);

export const getAlbumDataFromAlbums = createSelector(state => state.albums, getTerm, getPage, _getAlbumDataFromAlbums); 