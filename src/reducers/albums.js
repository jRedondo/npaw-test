import { SET_ALBUM_DATA } from './../actions';
import { createSelector } from 'reselect';

export const albums = (state = {}, action) => {
    switch (action.type) {
        case SET_ALBUM_DATA: {
            const{ term, albumData, page } = action.payload;
            return { ...state, [term]: { ...state[term], [page] : {  albumData }, albumDataDate: new Date() }};
        }

        default:
            return state;
    }
}

 export const getAlbumDataFromAlbums = 
        createSelector((state, term, page) => state[term] && state[term][page].albumData, albumData => albumData) 