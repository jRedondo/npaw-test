import { SET_TERM } from './../actions';

export const term = (state = {}, action) => {
    switch(action.type){
        case SET_TERM:
            return action.payload;
        default:
            return state;
    }
}