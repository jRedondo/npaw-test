import { SET_PAGE } from './../actions';

export const page = (state = {}, action) => {
    switch(action.type){
        case SET_PAGE:
            return action.payload;
        default:
            return state;
    }
}