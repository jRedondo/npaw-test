

const transformAlbum = (data, from, to) => (
    data.results.filter((item,i) => i >= from && i <= to).map(item=>(
        {
            author: item.artistName,
            artwork: item.artworkUrl100,
            big_artwork: item.artworkUrl100.replace('100x100','200x200'),
            album: item.collectionName,
        }
    )) 
);

export default transformAlbum;