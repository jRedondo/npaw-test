import transformAlbum from './../services/transformAlbum'
export const SET_TERM = 'SET_TERM';
export const SET_ALBUM_DATA = 'SET_ALBUM_DATA';

export const SET_PAGE = 'SET_PAGE';
export const SET_PAGES = 'SET_PAGES';

export const GET_ALBUM = 'GET_ALBUM';
export const SET_ALBUM = 'SET_ALBUM';

const setTerm = payload => ({type: SET_TERM, payload });
const setAlbumData = payload => ({ type: SET_ALBUM_DATA, payload });

const setPage = payload => ({type: SET_PAGE, payload });
const setPages = payload => ({type: SET_PAGES, payload });

const url= "https://itunes.apple.com/search";
const limit= "200"; 
const app= 20; //albums by page

export const setItunesTerm = (payload, page) => {
    return (dispatch, getState) => {
        const api_itunes = `${url}?term=${payload}&limit=${limit}&entity=album`; 

        dispatch(setTerm(payload));
        dispatch(setPage(page));

        const state = getState();
        const date = state.albums[payload] && state.albums[payload].albumDataDate;
        const isPage = state.albums[payload] && state.albums[payload][page] && page;
        const changePage = state.albums[payload] && state.albums[payload][page];

        const now = new Date();

        if (page > 1 && !isPage){
            dispatch(setPage(page-1));
            return;
        }else if (changePage && date && (now - date) < 1*60*1000){
            dispatch(setAlbumData({term: payload, albumData:state.albums[payload][page].albumData , page: page }));
            return;
        } 
        return fetch(api_itunes)
            .then(
                data => (data.json())
            ).then(
                album_list => {
                    const count = (album_list.resultCount > 0 ? album_list.resultCount : 1);
                    const pages = parseInt(count % app !== 0 ? count / app + 1 : count / app, 10);
                    
                    dispatch(setPages(pages));

                    for (var i = 1; i <= pages; i++) { 
                        var to = i * app - 1;   
                        var from = (to - (app -1 ));

                        var albumData = transformAlbum(album_list, from, to);
                        dispatch(setAlbumData({term: payload, albumData, page: i }));
                    }
                }
            );     
    }
};
