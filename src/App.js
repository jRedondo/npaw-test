import React, { Component } from 'react';
import AlbumListContainer from './containers/AlbumListContainer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

import './reset.css';
import './App.css';

class App extends Component {

  render() {

    return (
      <div>
        <div className="App">
          <AppBar position="static" color="default">
            <Toolbar>
              <Typography variant="title" color="inherit">
                NPAW - React/Redux test
              </Typography>
            </Toolbar>
          </AppBar>
          <AlbumListContainer></AlbumListContainer>
        </div>
      </div>
    );
  }
}

export default App;
