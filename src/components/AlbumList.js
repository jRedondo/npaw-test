import React from 'react';
import PropTypes from 'prop-types';
import Album from './Album';
import Paginator from './Paginator';
import View from './View';
import Search from './Search';
import { Grid, Row, Col} from 'react-flexbox-grid';
import CircularProgress from '@material-ui/core/CircularProgress';
import './style.css';

    const renderAlbums = albums => {
        return (albums.map((album,index) => ( 
            <div className="item col-xs-3 col-lg-3" key={index}>
                <Album                         
                    artwork={album.big_artwork} 
                    author={album.author}
                    album={album.album}>
                </Album>                
            </div>
        )));
    };

    const renderProgress = () =>{
        return <CircularProgress size={100} className="loading" />;
    };

    const AlbumList = ({ term, onInputUpdate, onPageUpdate, albumData=null, page, pages }) =>(
        <Grid>
            <Row>
                <Col xs={12} sm={4} md={4}> 
                    <Search onInputUpdate={onInputUpdate} />                                                         
                </Col>
                <Col xs={12} sm={12} md={12}>
                    <Row className="switchCont">
                        <Col xs={3} sm={3} md={3} className="searchTitle">
                            <h1>Search results</h1>
                        </Col>
                        <View />
                    </Row>
                </Col>
                <Col xs={12} sm={12} md={12} className="paginatorCont">
                    <Paginator page={page} pages={pages} onPageUpdate={onPageUpdate}/>
                </Col>
            </Row>           
            <Row id="albumsList" className="albumsCont grid-group-wrapper">
                {albumData ? renderAlbums(albumData) : renderProgress()}
            </Row>
        </Grid>      
    );

    AlbumList.propTypes ={
        page : PropTypes.number.isRequired,
        pages : PropTypes.number.isRequired,
        term: PropTypes.string.isRequired,
        onPageUpdate: PropTypes.func.isRequired,
        onInputUpdate: PropTypes.func.isRequired,
        albumData: PropTypes.array,
    }

export default AlbumList;