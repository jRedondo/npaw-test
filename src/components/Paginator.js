import React from 'react';
import PropTypes from 'prop-types';
import { Col, Row } from 'react-flexbox-grid';
import Button from '@material-ui/core/Button';

const goPage = (page, onPageUpdate) => {
    onPageUpdate(page);
};

const Paginator = ({page, pages = 1, onPageUpdate}) => (
        <Row className="paginator">
            <Col xs={2} sm={2} md={2}  className="totalPagesCont">
                <span className="totalPages">Page {page} of {pages}</span>
            </Col>
            <Col xs={2} sm={2} md={2}  className="paginatorButonsCont">
                <Button variant="outlined" size="small" className="prev" onClick={() => goPage(page>1?page-1:1, onPageUpdate) }>
                <i className="fas fa-angle-left"></i>
                </Button>
                <Button variant="outlined" size="small" className="next" onClick={() => goPage(page+1, onPageUpdate) }>
                <i className="fas fa-angle-right"></i>
                </Button>
            </Col>
        </Row>
)

Paginator.propTypes = {
    page: PropTypes.number.isRequired,
    pages: PropTypes.number.isRequired,
    onPageUpdate: PropTypes.func.isRequired,
}

export default Paginator