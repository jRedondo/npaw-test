import React from 'react';
import PropTypes from 'prop-types';
import AlbumItem from './AlbumItem'
import './styles.css'

const Album = ({artwork, author, album }) => {
           
    return (
        <AlbumItem artwork={artwork} author={author} album={album}/>
    );
};

Album.propTypes = {
    artwork: PropTypes.string.isRequired,
    author: PropTypes.string.isRequired,
    album: PropTypes.string.isRequired,
};

export default Album;