import React from 'react';
import PropTypes from 'prop-types';

const AlbumItem = ({artwork, author, album}) => (
    <div className="thumbnail clearfix">
        <img src={artwork} alt="artwork" />
        <div className="caption">
            <p className='albumTitle'>{album}</p>
            <p className='albumAuthor'>{author}</p>
        </div>
    </div>
);

AlbumItem.propTypes = {
    artwork: PropTypes.string.isRequired,
    author: PropTypes.string.isRequired,
    album: PropTypes.string.isRequired,
}

export default AlbumItem;