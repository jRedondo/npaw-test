import React from 'react';
import { Col } from 'react-flexbox-grid';
import Switch from '@material-ui/core/Switch';

const handleView = name => event => {
    const element = document.getElementById("albumsList");

    if(event.target.checked){
        element.classList.remove("grid-group-wrapper");
        element.classList.add("list-group-wrapper");
    }else{
        element.classList.remove("list-group-wrapper");
        element.classList.add("grid-group-wrapper");
    }
};

const View = () => (
    <Col xs={6} sm={6} md={3} className="switch">   
        <i className="fas fa-th"></i>  
        <Switch
            checked={this.checked}
            onChange={handleView('checked')}
            value="checkedB"
            color="primary"
        />                       
        <i className="fas fa-list"></i>     
    </Col>
)

export default View