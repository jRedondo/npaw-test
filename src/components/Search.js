import React from 'react';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';

const doSearch = (evt, onInputUpdate) => {
    const searchText = evt.target.value;

    if (searchText === "") return;
    if(this.timeout) clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {

        onInputUpdate(searchText);
    }, 300);
};

const Search = ({ onInputUpdate }) => (
    <TextField
        id="term"
        label=""
        placeholder='Search'
        InputLabelProps={{
            shrink: true,
        }}
        InputProps={{
            startAdornment: <InputAdornment position="start"><i className="fas fa-search"></i></InputAdornment>,
        }}
        onChange={evt => doSearch(evt, onInputUpdate)}
        fullWidth
        margin="normal"
    />  
)

Search.propTypes = {
    onInputUpdate: PropTypes.func.isRequired,
}

export default Search